#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main()
{
	vector<int> count = {1,2,3,4,5,6,7,8,9,10};
	string Hello = "Hello World";
	int loop_length = count.size();

	for (auto i = 0; i < loop_length ;  i++)
	{
		for (int num: count) // For each integer in the vector count
		{
			cout << num << ' ';
		}
	cout << '\n';
	count.pop_back();
	}
	cout << Hello << '\n';
	return 0;
}


